console.log("ES6 Updates:");

	//[Section] Exponent Operator
		//before ES6
		const firstNum = 8 ** 2;
		console.log(firstNum);

		//ES6
		/*
			Syntax: Math.pow(base, exponent);
		*/
		const secondNum = Math.pow(8,2);
		console.log(secondNum);

	//[Section] Template Literals
		//allows us to write strings without using concatenation operator (+);
		//greatly helps with code readbility

	let name = "John";
		//Before ES6
		let message = "Hello " + name + "! Welcome to programming."
		console.log(message);

		//After ES6
		//uses backticks(``)

		message = `Hello ${name}! Welcome to programming.`
		console.log(message);

		//Template literals allows us to perform operations

		const interestRate = 0.1;
		const principal = 1000;

		console.log(`The interest on your savings account is: ${interestRate*principal}.`);


	//[Section] Array Destructuring
		//allows us to unpack elements in an array into distinct variables
		//allows us to name array elements with variables instead of using index number
		/*
			Syntax:
				let/const [variableNameA, variableNameB, . . ] = arrayName;
		*/

	const fullName = ["Juan", "Dela", "Cruz"];

		//before ES6
		let firstName = fullName[0];
		let middleName = fullName[1];
		let lastName = fullName[2];
		console.log(`Hello ${firstName} ${middleName} ${lastName}!`);

		//after ES6 updates
		const [ mName, fName, lName] = fullName;

		console.log(fName);
		console.log(mName);
		console.log(lName);


		//Mini-activity
			//Array destructuring
		let array = [1, 2, 3, 4, 5];

			//destructure the array into 5 different variables;

	//[Section] Object Destructuring
		//allows us to unpack properties of objects into distinct variables
		/*
			Syntax:
				let/conts {propertyNameA, propertyNameB, . . .} = objectName
		*/

		const person = {
			givenName: "Jane",
			maidenName: "Dela",
			familyName: "Cruz"
		}
			//Before ES6

			let gName = person.givenName;
			let midName = person.maidenName;
			let famName = person.familyName;
			console.log(gName);
			console.log(midName);
			console.log(famName); 

			//after ES6 update

			let { maidenName, familyName, givenName} = person
			console.log("Object destructuring after ES6")
			console.log(givenName);
			console.log(maidenName);
			console.log(familyName);

		//[Section] Arrow Functions
			//compact alternative syntax to traditional functions

		const hello = () =>{
			console.log("Hello world!")
		}

		hello();

		/*//function expression
			const hello = function(){
				console.log("Hello World!")
			}

		//function declaration
			function hello(){
				console.log("Hello World!")
			}*/

		//[Section] Implicit Return
			/*
				there are instances when you can omit return statement
				this works because even withiout using return keyword
			*/

		const add = (x, y) => {
			
			return x+y
		};

		console.log("Implicit return:")
		console.log(add(12, 2));

		const subtract = (x,y) => {
			return x-y
		};
		console.log(subtract(10, 5));


	//[Section] Default Function Argument Value
		/*const greet = (firstName = "firstName", lastName = "lastName") => {
			return `Good afterrnoon, ${firstName} ${lastName}!`;
		}	

		console.log(greet("Chris", "Mortel"));*/

		function greet(firstName = "fullName", lastName = "lastName"){
			return `Good afterrnoon, ${firstName} ${lastName}!`
		}

		console.log(greet());

	//[Section] Class-based Object Blueprints
		//allows us to create/instantiation of objects using classes blueprints
		//create class
			//constructor is a special method of a class for creating/initializing an object of the class
		/*
			Syntax:
			class className{
				constructor(objectValueA, objectValueB, . . . ){
					this.objectPropertyA = objectValueA;
					this.objectPropertyB = objectValueB

				}

			}
		
		*/


		class Car{
			constructor(carBrand, carName, carYear){
				this.carBrand = carBrand;
				this.carName =carName;
				this.carYear = carYear;
			}
		}

		let car = new Car("Toyota", "Hilux-pickup", 2015)
		console.log(car);

		car.carBrand = "Nissan"
		console.log(car);